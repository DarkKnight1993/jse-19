package ru.tsc.goloshchapov.tm.enumerated;

import ru.tsc.goloshchapov.tm.comparator.ComparatorByCreated;
import ru.tsc.goloshchapov.tm.comparator.ComparatorByName;
import ru.tsc.goloshchapov.tm.comparator.ComparatorByStartDate;
import ru.tsc.goloshchapov.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

public enum Sort {

    NAME("Sort by name", ComparatorByName.getInstance()),
    CREATED("Sort by created", ComparatorByCreated.getInstance()),
    START_DATE("Sort by date start", ComparatorByStartDate.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance());

    private final String displayName;

    private final Comparator comparator;

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

}
