package ru.tsc.goloshchapov.tm.command;

import ru.tsc.goloshchapov.tm.exception.empty.EmptyNameException;
import ru.tsc.goloshchapov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.goloshchapov.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected void showProject(Project project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("[SELECTED PROJECT]");
        System.out.println(
                "Id: " + project.getId() +
                        "\nName: " + project.getName() +
                        "\nDescription: " + project.getDescription() +
                        "\nStatus: " + project.getStatus().getDisplayName()
        );
        System.out.println("[END PROJECT]");
    }

    protected Project createProject(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return new Project(name, description);
    }

}
