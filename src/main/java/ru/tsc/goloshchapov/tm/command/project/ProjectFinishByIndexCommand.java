package ru.tsc.goloshchapov.tm.command.project;

import ru.tsc.goloshchapov.tm.command.AbstractProjectCommand;
import ru.tsc.goloshchapov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.goloshchapov.tm.model.Project;
import ru.tsc.goloshchapov.tm.util.TerminalUtil;

public final class ProjectFinishByIndexCommand extends AbstractProjectCommand {
    @Override
    public String name() {
        return "project-finish-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Finish project by index";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[FINISH PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = serviceLocator.getProjectService().finishByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("[OK]");
    }

}
