package ru.tsc.goloshchapov.tm.command.auth;

import ru.tsc.goloshchapov.tm.command.AbstractAuthCommand;
import ru.tsc.goloshchapov.tm.util.TerminalUtil;

public final class AuthChangePasswordCommand extends AbstractAuthCommand {
    @Override
    public String name() {
        return "change-password";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Change user's password";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().changePassword(userId, password);
        System.out.println("[OK]");
    }
}
