package ru.tsc.goloshchapov.tm.repository;

import ru.tsc.goloshchapov.tm.api.IRepository;
import ru.tsc.goloshchapov.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> entities = new ArrayList<>();

    @Override
    public void add(E entity) {
        entities.add(entity);
    }

    @Override
    public void remove(final E entity) {
        entities.remove(entity);
    }

    @Override
    public boolean existsById(final String id) {
        final E entity = findById(id);
        return entity != null;
    }

    @Override
    public boolean existsByIndex(final Integer index) {
        if (index < 0) return false;
        return index < findAll().size();
    }

    @Override
    public List<E> findAll() {
        return entities;
    }

    @Override
    public List<E> findAll(final Comparator<E> comparator) {
        final List<E> sortedEntities = new ArrayList<>(entities);
        sortedEntities.sort(comparator);
        return sortedEntities;
    }

    @Override
    public E findById(final String id) {
        for (E entity : entities) {
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public E findByIndex(final Integer index) {
        return entities.get(index);
    }

    @Override
    public E removeById(final String id) {
        final E entity = findById(id);
        if (entity == null) return null;
        entities.remove(entity);
        return entity;
    }

    @Override
    public E removeByIndex(final Integer index) {
        final E entity = findByIndex(index);
        if (entity == null) return null;
        entities.remove(entity);
        return entity;
    }

    @Override
    public void clear() {
        entities.clear();
    }

}
