package ru.tsc.goloshchapov.tm.repository;

import ru.tsc.goloshchapov.tm.api.repository.ITaskRepository;
import ru.tsc.goloshchapov.tm.enumerated.Status;
import ru.tsc.goloshchapov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @Override
    public Task findByName(final String userId, final String name) {
        for (Task task : entities) {
            if (!userId.equals(task.getUserId())) continue;
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task removeByName(final String userId, final String name) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        entities.remove(task);
        return task;
    }

    @Override
    public Task startById(final String userId, final String id) {
        final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByIndex(final String userId, final Integer index) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByName(final String userId, final String name) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishById(final String userId, final String id) {
        final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByIndex(final String userId, final Integer index) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByName(final String userId, final String name) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task changeStatusById(final String userId, final String id, final Status status) {
        final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(final String userId, final Integer index, final Status status) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByName(final String userId, final String name, final Status status) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public List<Task> findAllTaskByProjectId(final String userId, final String projectId) {
        List<Task> listByProject = new ArrayList<>();
        for (Task task : entities) {
            if (!userId.equals(task.getUserId())) continue;
            if (projectId.equals(task.getProjectId())) listByProject.add(task);
        }
        return listByProject;
    }

    @Override
    public void removeAllTaskByProjectId(final String userId, final String projectId) {
        List<Task> listByProject = findAllTaskByProjectId(userId, projectId);
        for (Task task : listByProject) {
            entities.remove(task);
        }
    }

    @Override
    public Task bindTaskToProjectById(final String userId, final String projectId, final String taskId) {
        final Task task = findById(userId, taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskById(final String userId, final String taskId) {
        final Task task = findById(userId, taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

}
