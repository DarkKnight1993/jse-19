package ru.tsc.goloshchapov.tm.repository;

import ru.tsc.goloshchapov.tm.api.IOwnerRepository;
import ru.tsc.goloshchapov.tm.model.AbstractOwnerEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E> implements IOwnerRepository<E> {

    @Override
    public void add(final String userId, E entity) {
        entity.setUserId(userId);
        add(entity);
    }

    @Override
    public void remove(final String userId, final E entity) {
        if (!userId.equals(entity.getUserId())) return;
        remove(entity);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        final E entity = findById(userId, id);
        return existsById(id);
    }

    @Override
    public boolean existsByIndex(final String userId, final Integer index) {
        if (index < 0) return false;
        return index < findAll(userId).size();
    }

    @Override
    public List<E> findAll(final String userId) {
        final List<E> entitiesWithUserId = new ArrayList<>();
        for (final E entity : entities) {
            if (userId.equals(entity.getUserId())) entitiesWithUserId.add(entity);
        }
        return entitiesWithUserId;
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        final List<E> sortedEntitiesWithUserId = new ArrayList<>();
        for (final E entity : entities) {
            if (userId.equals(entity.getUserId())) sortedEntitiesWithUserId.add(entity);
        }
        sortedEntitiesWithUserId.sort(comparator);
        return sortedEntitiesWithUserId;
    }

    @Override
    public E findById(final String userId, final String id) {
        for (E entity : entities) {
            if (!userId.equals(entity.getUserId())) continue;
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public E findByIndex(final String userId, final Integer index) {
        final List<E> entitiesByUserId = findAll(userId);
        return entitiesByUserId.get(index);
    }

    @Override
    public E removeById(final String userId, final String id) {
        final E entity = findById(userId, id);
        if (entity == null) return null;
        remove(entity);
        return entity;
    }

    @Override
    public E removeByIndex(final String userId, final Integer index) {
        final E entity = findByIndex(userId, index);
        if (entity == null) return null;
        remove(entity);
        return entity;
    }

    @Override
    public void clear(final String userId) {
        final List<E> entitiesForRemoveWithUserId = findAll(userId);
        entities.removeAll(entitiesForRemoveWithUserId);
    }

}
