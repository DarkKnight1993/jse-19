package ru.tsc.goloshchapov.tm.service;

import ru.tsc.goloshchapov.tm.api.IOwnerRepository;
import ru.tsc.goloshchapov.tm.api.IOwnerService;
import ru.tsc.goloshchapov.tm.exception.empty.EmptyIdException;
import ru.tsc.goloshchapov.tm.exception.empty.EmptyIndexException;
import ru.tsc.goloshchapov.tm.exception.empty.EmptyUserIdException;
import ru.tsc.goloshchapov.tm.exception.entity.EntityNotFoundException;
import ru.tsc.goloshchapov.tm.model.AbstractOwnerEntity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity> extends AbstractService<E> implements IOwnerService<E> {

    IOwnerRepository<E> ownerRepository;

    public AbstractOwnerService(IOwnerRepository<E> ownerRepository) {
        super(ownerRepository);
        this.ownerRepository = ownerRepository;
    }

    @Override
    public void add(String userId, E entity) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (entity == null) throw new EntityNotFoundException();
        ownerRepository.add(userId, entity);
    }

    @Override
    public void remove(String userId, E entity) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (entity == null) throw new EntityNotFoundException();
        ownerRepository.remove(userId, entity);
    }

    @Override
    public boolean existsById(String userId, String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return ownerRepository.existsById(userId, id);
    }

    @Override
    public boolean existsByIndex(String userId, Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return ownerRepository.existsByIndex(userId, index);
    }

    @Override
    public List<E> findAll(String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return ownerRepository.findAll(userId);
    }

    @Override
    public List<E> findAll(String userId, Comparator<E> comparator) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (comparator == null) return Collections.emptyList();
        return ownerRepository.findAll(userId, comparator);
    }

    @Override
    public E findById(String userId, String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return ownerRepository.findById(userId, id);
    }

    @Override
    public E findByIndex(String userId, Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return ownerRepository.findByIndex(userId, index);
    }

    @Override
    public E removeById(String userId, String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return ownerRepository.removeById(userId, id);
    }

    @Override
    public E removeByIndex(String userId, Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return ownerRepository.removeByIndex(userId, index);
    }

    @Override
    public void clear(String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        ownerRepository.clear(userId);
    }

}
