package ru.tsc.goloshchapov.tm.service;

import ru.tsc.goloshchapov.tm.api.repository.ICommandRepository;
import ru.tsc.goloshchapov.tm.api.service.ICommandService;
import ru.tsc.goloshchapov.tm.command.AbstractCommand;
import ru.tsc.goloshchapov.tm.exception.empty.EmptyArgException;
import ru.tsc.goloshchapov.tm.exception.empty.EmptyNameException;

import java.util.Collection;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public AbstractCommand getCommandByName(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return commandRepository.getCommandByName(name);
    }

    @Override
    public AbstractCommand getCommandByArg(String arg) {
        if (arg == null || arg.isEmpty()) throw new EmptyArgException();
        return commandRepository.getCommandByArg(arg);
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    public Collection<AbstractCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @Override
    public Collection<String> getListCommandName() {
        return commandRepository.getCommandNames();
    }

    @Override
    public Collection<String> getListCommandArg() {
        return commandRepository.getCommandArg();
    }

    @Override
    public void add(AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

}
